﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace array
{
    public partial class Form1 : Form
    {
        char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f'};
        
        public Form1()
        {
            InitializeComponent();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.AddFirst('X');
            stop1.Stop();
            stop2.Start();
            arrayList.Insert(0, 'X');
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label1.Text = span1.ToString() + "  " + span2.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.AddBefore(getMiddleNode(linkedList).Next, 'X');
            stop1.Stop();
            stop2.Start();
            arrayList.Insert((arrayList.Count - 1 )/2, 'X');
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label2.Text = span1.ToString() + "  " + span2.ToString();

        }


        LinkedListNode<char> getMiddleNode(LinkedList<char> l)
        {
            int length = 0;
            LinkedListNode<char> node = l.First;
            LinkedListNode<char> middle = l.First;
            while(node.Next != null)
            {
                length++;
                if (length % 2 == 0)
                {
                    middle = middle.Next;
                }
                node = node.Next;

            }

            return middle;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.AddLast('X');
            stop1.Stop();
            stop2.Start();
            arrayList.Insert((arrayList.Count - 1), 'X');
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label3.Text = span1.ToString() + "  " + span2.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.RemoveFirst();
            stop1.Stop();
            stop2.Start();
            arrayList.RemoveAt(0);
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;
/*
           foreach(Char c in linkedList)
           {
               listBox1.Items.Add(c);
           }
           */

            label4.Text = span1.ToString() + "  " + span2.ToString();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.Remove(getMiddleNode(linkedList).Next);
            stop1.Stop();
            stop2.Start();
            arrayList.RemoveAt((arrayList.Count - 1) / 2);
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label5.Text = span1.ToString() + "  " + span2.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            linkedList.RemoveLast();
            stop1.Stop();
            stop2.Start();
            arrayList.RemoveAt((arrayList.Count - 1));
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label6.Text = span1.ToString() + "  " + span2.ToString();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            var o = linkedList.First();
            stop1.Stop();
            stop2.Start();
            var p = arrayList[0];
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;

            label7.Text = span1.ToString() + "  " + span2.ToString();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            var o = getMiddleNode(linkedList).Next;
            stop1.Stop();
            stop2.Start();
            var p = arrayList[(arrayList.Count - 1) / 2];
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label8.Text = span1.ToString() + "  " + span2.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            ArrayList arrayList = new ArrayList(chars);
            LinkedList<char> linkedList = new LinkedList<char>(chars);
            Stopwatch stop1 = new Stopwatch();
            Stopwatch stop2 = new Stopwatch();
            stop1.Start();
            var o = linkedList.Last();
            stop1.Stop();
            stop2.Start();
            var p = arrayList[arrayList.Count - 1];
            stop2.Stop();

            TimeSpan span1 = stop1.Elapsed;
            TimeSpan span2 = stop2.Elapsed;


            label9.Text = span1.ToString() + "  " + span2.ToString();
        }
    }
}
